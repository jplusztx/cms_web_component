# 组件库文档地址

https://jplusztx.gitee.io/cms_web_component/

# 如何启动查看演示?

1. `npm install` / `npm i`
2. `npm run dev`

# 如何查看组件库

1. `npm run storybook`

# 关于 WebComponent 可以查看以下地址介绍

- [Web Component - 自定义元素](https://flowus.cn/challenge/share/7a4c21e6-3bc6-4970-b60e-c1244382821d)

# 提交代码过程

1. 通过 master 分支创建自己的开发分支（命名最好带上自己的名字缩写）
2. 做出需要提交的修改之后，提起`pull request`合并到`master`分支
3. `git commit`设置了格式校验，提交信息中必须加上固定的前缀标识这次提交主要干了什么，我们使用如下几个前缀
   - `fix: 修复了什么bug`
   - `style: 做了哪部分样式修改 / 代码格式修改`
   - `test: 单元测试`
   - `feat: 新增了什么功能`
   - `docs: 文档更新`
   - `refactor: 代码重构`
   - `chore: 构建工具或辅助工具变动`

- 具体使用样例：`git commit -m "feat: 新增CmsButton组件"`
