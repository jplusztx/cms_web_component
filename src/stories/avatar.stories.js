import { html } from 'lit-html';
import '../component/avatar/index';

export default {
  title: 'Web-Component/Avatar',
  parameters: {
    docs: {
      description: {
        component:
          'Avatar 组件可以用来代表人物或对象， 支持使用图片、图标或者文字作为 Avatar。<br>使用`shape`和`size`属性来设置Avatar的形状和大小。',
      },
    },
  },
  argTypes: {
    shape: {
      control: { type: 'select' },
      options: ['square', 'circle'],
      defaultValue: 'circle',
      description: '设置头像形状',
    },
    size: {
      control: { type: 'select' },
      options: ['large', 'default', 'medium', 'small', 'auto'],
      description: '设置头像形状大小',
    },
    fit: {
      control: { type: 'select' },
      options: ['fill', 'contain', 'cover', 'scale-down', 'none', 'initial', 'inherit'],
      description: '当展示类型为图片的时候，设置图片如何适应容器框',
    },
    alt: {
      defaultValue: 'error',
      description: '描述图像的替换文本',
    },
    src: {
      description: '图片头像的资源地址',
    },
  },
};

const Template = args =>
  html`<cms-avatar
    shape=${args.shape}
    size=${args.size}
    src=${args.src}
    fit=${args.fit}
    lazy-load=${args.lazyload}
    alt=${args.alt}
    error=${args.error}
    load=${args.load}
  ></cms-avatar>`;

export const Display = Template.bind({});
Display.args = {
  shape: 'square',
  src: 'http://localhost:6006/assets/autoImage.jpeg',
  lazyload: true,
  alt: 'fff',
};
Display.decorators = [story => html`<div>${story()}</div>`];

export const Function = Template.bind({});
Function.args = {
  error: funcError(),
  load: funcLoad(),
};

function funcError() {
  console.log('error');
}

function funcLoad() {
  console.log('load error');
}
