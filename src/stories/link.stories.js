import { html } from 'lit-html';
import '../component/link/index';

export default {
  title: 'Web-Component/Link',
  parameters: {
    docs: {
      description: {
        component:
          '文字超链接。可以使用`underline`来定义文字链接样式，使用`disabled`属性来定义文字链接是否被禁用。',
      },
    },
  },
  argTypes: {
    type: {
      control: { type: 'radio' },
      options: ['default', 'info', 'success', 'warning', 'danger'],
      description: '类型',
      table: { defaultValue: { summary: 'default' } },
    },
    underline: {
      description: '是否有下划线',
      table: { defaultValue: { summary: 'true' } },
    },
    disabled: {
      description: '是否禁用状态',
      table: { defaultValue: { summary: 'false' } },
    },
    href: {
      control: { typr: 'text' },
      description: '原生 href 属性',
    },
  },
};

const Template = ({ type, underline, disabled, href }) =>
  html`<cms-link type=${type} underline=${underline} disabled=${disabled} href=${href}
    >link组件展示</cms-link
  >`;

export const Default = Template.bind({});
Default.storyName = 'Default';
Default.args = {
  type: 'default',
  underline: true,
  disabled: false,
  href: 'www.baidu.com',
};

export const TypeSuccess = Template.bind({});
TypeSuccess.storyName = 'Type: success';
TypeSuccess.args = {
  type: 'success',
  underline: true,
  disabled: false,
};

export const TypeInfo = Template.bind({});
TypeInfo.storyName = 'Type: info';
TypeInfo.args = {
  type: 'info',
  underline: true,
  disabled: false,
};

export const TypeWarning = Template.bind({});
TypeWarning.storyName = 'Type: warning';
TypeWarning.args = {
  type: 'warning',
  underline: true,
  disabled: false,
};

export const TypeDanger = Template.bind({});
TypeDanger.storyName = 'Type: danger';
TypeDanger.args = {
  type: 'danger',
  underline: true,
  disabled: false,
};

export const Underline = Template.bind({});
Underline.storyName = 'Underline: false';
Underline.args = {
  type: 'default',
  underline: false,
  disabled: false,
  href: 'www.baidu.com',
};

export const Disabled = Template.bind({});
Disabled.storyName = 'Disabled';
Disabled.args = {
  type: 'default',
  underline: true,
  disabled: true,
};

export const DisabledSuccess = Template.bind({});
DisabledSuccess.storyName = 'Disabled: success';
DisabledSuccess.args = {
  type: 'success',
  underline: true,
  disabled: true,
};

export const DisabledInfo = Template.bind({});
DisabledInfo.storyName = 'Disabled: info';
DisabledInfo.args = {
  type: 'info',
  underline: true,
  disabled: true,
};

export const DisabledWarning = Template.bind({});
DisabledWarning.storyName = 'Disabled: warning';
DisabledWarning.args = {
  type: 'warning',
  underline: true,
  disabled: true,
};

export const DisabledDanger = Template.bind({});
DisabledDanger.storyName = 'Disabled: danger';
DisabledDanger.args = {
  type: 'danger',
  underline: true,
  disabled: true,
};
