import { html } from 'lit-html';
import '../component/divider/index';

export default {
  title: 'Web-Component/Divider',
  parameters: {
    docs: {
      description: {
        component:
          '区隔内容的分割线。对不同段落的文本进行分割。也可以在分割线上自定义文本内容，通过`content-position`属性改变文本内容位置。也可以使用`border-style`属性改变分隔符的样式。',
      },
    },
  },
  argTypes: {
    direction: {
      control: { type: 'radio' },
      options: ['horizontal', 'vertical'],
      description: '设置分割线方向',
      table: { defaultValue: { summary: 'horizontal' } },
    },
    'content-position': {
      control: { type: 'radio' },
      options: ['left', 'center', 'right'],
      description: '自定义分隔线内容的位置',
      table: { defaultValue: { summary: 'center' } },
    },
    'border-style': {
      control: { type: 'radio' },
      options: ['solid', 'dashed'],
      description: '设置分隔符样式',
      table: { defaultValue: { summary: 'solid' } },
    },
  },
};

const Template = obj =>
  html`<cms-divider
    direction=${obj['direction']}
    content-position=${obj['content-position']}
    border-style=${obj['border-style']}
    >${obj['slot']}</cms-divider
  >`;

export const Default = Template.bind({});
Default.args = {
  direction: 'horizontal',
};

export const StyleDashed = Template.bind({});
StyleDashed.storyName = 'BorderStyle: dashed';
StyleDashed.args = {
  direction: 'horizontal',
  'border-style': 'dashed',
};

export const PositionLeft = Template.bind({});
PositionLeft.storyName = 'ContentPosition: left';
PositionLeft.args = {
  direction: 'horizontal',
  'border-style': 'solid',
  'content-position': 'left',
  slot: 'left',
};

export const PositionCenter = Template.bind({});
PositionCenter.storyName = 'ContentPosition: center';
PositionCenter.args = {
  direction: 'horizontal',
  'border-style': 'solid',
  'content-position': 'center',
  slot: 'center',
};

export const PositionRight = Template.bind({});
PositionRight.storyName = 'ContentPosition: right';
PositionRight.args = {
  direction: 'horizontal',
  'border-style': 'solid',
  'content-position': 'right',
  slot: 'right',
};

export const DirectionVertical = Template.bind({});
DirectionVertical.storyName = 'Direction: vertical';
DirectionVertical.args = {
  direction: 'vertical',
};
DirectionVertical.decorators = [
  story =>
    html`
      <div>
        <span>divider</span>
        ${story()}
        <span>组件</span>
        ${story()}
        <span>展示</span>
      </div>
    `,
];
