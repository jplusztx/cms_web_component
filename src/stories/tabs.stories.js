import { html } from 'lit-html';
import '../component/tabs/index';
import '../component/tabs/item';
import '../component/tag/index';
import '../component/divider/index';

export default {
  title: 'Web-Component/Tabs',
  parameters: {
    docs: {
      description: {
        component: '分隔内容上有关联但属于不同类别的数据集合。',
      },
    },
  },
  argTypes: {
    active: {
      description: '组件加载后所在的tab',
    },
  },
};

const Template = ({ active }) => {
  return html`
    <cms-tabs active=${active}>
      <cms-tabs-item label="tag组件展示">
        <cms-tag size="large">Tag组件</cms-tag>
      </cms-tabs-item>
      <cms-tabs-item label="alert组件展示">
        <cms-alert message="alert组件效果展示" type="error">alert组件</cms-alert>
      </cms-tabs-item>
      <cms-tabs-item label="divider组件展示">
        <cms-divider content-position="left">divider组件</cms-divider>
      </cms-tabs-item>
    </cms-tabs>
  `;
};

export const Active = Template.bind({});
Active.args = {
  active: 1,
};
