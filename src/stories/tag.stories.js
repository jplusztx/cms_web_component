import { html } from 'lit-html';
import '../component/tag/index';

export default {
  title: 'Web-Component/Tag',
  parameters: {
    docs: {
      description: {
        component:
          '用于标记和选择。可以使用`type`，`size`，`round`来定义标签样式，使用`closable`属性来定义标签是否可关闭。',
      },
    },
  },
  argTypes: {
    round: {
      control: { type: 'boolean' },
      description: 'Tag 是否为圆形',
      table: { defaultValue: { summary: 'false' } },
    },
    closable: {
      description: '是否可关闭',
      table: { defaultValue: { summary: 'false' } },
    },
    size: {
      control: { type: 'radio' },
      options: ['small', 'default', 'large'],
      table: { defaultValue: { summary: 'default' } },
      description: '尺寸',
    },
    type: {
      control: { type: 'radio' },
      options: ['default', 'success', 'info', 'warning', 'danger'],
      table: { defaultValue: { summary: 'default' } },
      description: '类型',
    },
  },
};

const Template = ({ type, size, round, closable }) =>
  html`<cms-tag type=${type} size=${size} round=${round} closable=${closable}
    >tag组件展示</cms-tag
  >`;

export const Default = Template.bind({});
Default.args = {
  type: 'default',
  size: 'default',
  closable: false,
  round: false,
};

export const TypeSuccess = Template.bind({});
TypeSuccess.storyName = 'Type: success';
TypeSuccess.args = {
  type: 'success',
  size: 'default',
  closable: false,
  round: false,
};

export const TypeInfo = Template.bind({});
TypeInfo.storyName = 'Type: info';
TypeInfo.args = {
  type: 'info',
  size: 'default',
  closable: false,
  round: false,
};

export const TypeWarning = Template.bind({});
TypeWarning.storyName = 'Type: warning';
TypeWarning.args = {
  type: 'warning',
  size: 'default',
  closable: false,
  round: false,
};

export const TypeDanger = Template.bind({});
TypeDanger.storyName = 'Type: danger';
TypeDanger.args = {
  type: 'danger',
  size: 'default',
  closable: false,
  round: false,
};

export const RoundSuccess = Template.bind({});
RoundSuccess.storyName = 'Round: success';
RoundSuccess.args = {
  type: 'success',
  size: 'default',
  closable: false,
  round: true,
};

export const RoundInfo = Template.bind({});
RoundInfo.storyName = 'Round: info';
RoundInfo.args = {
  type: 'info',
  size: 'default',
  closable: false,
  round: true,
};

export const RoundWarning = Template.bind({});
RoundWarning.storyName = 'Round: warning';
RoundWarning.args = {
  type: 'warning',
  size: 'default',
  closable: false,
  round: true,
};

export const RoundDanger = Template.bind({});
RoundDanger.storyName = 'Round: danger';
RoundDanger.args = {
  type: 'danger',
  size: 'default',
  closable: false,
  round: true,
};

export const SizeSmall = Template.bind({});
SizeSmall.storyName = 'Size: Small';
SizeSmall.args = {
  type: 'default',
  size: 'small',
  closable: false,
  round: false,
};

export const SizeLarge = Template.bind({});
SizeLarge.storyName = 'Size: Large';
SizeLarge.args = {
  type: 'default',
  size: 'large',
  closable: false,
  round: false,
};

export const Closable = Template.bind({});
Closable.storyName = 'Closable: true';
Closable.args = {
  type: 'default',
  size: 'default',
  closable: true,
  round: false,
};
