import { html } from 'lit-html';
import '../component/tooltips/index';

export default {
  title: 'Web-Component/Tooltips',
};

const Template = args =>
  html`<cms-alert
    content=${args['content']}
    trigger=${args['trigger']}
    show=${args['show']}
    placement=${args['placement']}
    strategy=${args['strategy']}
    enabled=${args['enabled']}
    offset-x=${args['offset-x']}
    offset-y=${args['offset-y']}
    arrow=${args['arrow']}
    theme=${args['theme']}
    displaytime=${args['displaytime']}
    delaytime=${args['delaytime']}
  ></cms-alert>`;
