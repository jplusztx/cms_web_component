import { html } from 'lit-html';
import '../component/pagination/index';

export default {
  title: 'Web-Component/Pagination',
  parameters: {
    docs: {
      description: {
        component: '当数据量过多时，使用分页分解数据。',
      },
    },
  },
  argTypes: {
    'page-size': {
      description: '每页显示条目个数',
    },
    total: {
      description: '总条目数',
    },
    'current-page': {
      description: '当前页数',
    },
    'current-change': {
      description: 'currentPage 改变时会触发',
    },
    'prev-click': {
      description: '用户点击上一页按钮改变当前页后触发',
    },
    'next-click': {
      description: '用户点击下一页按钮改变当前页后触发',
    },
    'page-sizes': {
      description: '每页显示个数选择器的选项设置',
    },
  },
};

const Template = args =>
  html`<cms-pagination
    page-size=${args['page-size']}
    total=${args['total']}
    current-page=${args['current-page']}
    current-change=${args['current-change']}
    prev-click=${args['prev-click']}
    next-click=${args['next-click']}
    page-sizes=${args['page-sizes']}
  ></cms-pagination>`;

export const Total = Template.bind({});
Total.args = {
  total: 30,
  'page-size': 2,
  'page-sizes': [2, 4, 5, 10],
};

export const Change = Template.bind({});
Change.args = {
  total: 30,
  'page-size': 3,
  'current-change': currentChangeFunc(),
  'prev-click': prevClickFunc(),
  'next-click': nextClickFunc(),
};

function currentChangeFunc() {
  console.log('test-currentChangeCallback');
}
function prevClickFunc() {
  console.log('test-prevClickCallback');
}
function nextClickFunc() {
  console.log('test-nextClickCallback');
}
