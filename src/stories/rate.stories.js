import { html } from 'lit-html';
import '../component/rate/index';

export default {
  title: 'Web-Component/Rate',
  parameters: {
    docs: {
      description: {
        component: '用于评分。',
      },
    },
  },
  argTypes: {
    max: {
      description: '支持的最大评分值',
    },
    value: {
      description: '当前评分值',
    },
    half: {
      description: '是否支持半星',
    },
  },
};

const Template = ({ max, value, half }) =>
  html`<cms-rate max=${max} value=${value} half=${half} "></cms-rate>`;

export const Max = Template.bind({});
Max.args = {
  max: 4,
  value: 1,
  half: false,
};
