import { html } from 'lit-html';
import '../component/collapse/index';

export default {
  title: 'Web-Component/Collapse',
  parameters: {
    docs: {
      description: {
        component: '通过折叠面板收纳内容区域。',
      },
    },
  },
  argTypes: {
    disabled: {
      description: '是否可折叠',
      table: { defaultValue: { summary: 'false' } },
    },
    'default-display': {
      description: '是否默认展开',
      table: { defaultValue: { summary: 'false' } },
    },
  },
};

const Template = args =>
  html` <cms-collapse disabled=${args['disabled']} default-display=${args['default-display']}>
    <div>cllapse组件</div>
    <div>页面效果</div>
  </cms-collapse>`;

export const Display = Template.bind({});
Display.args = {
  disabled: false,
  'default-display': false,
};
