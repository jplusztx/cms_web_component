import { html } from 'lit-html';
import '../component/loading/index';

export default {
  title: 'Web-Component/Loading',
  parameters: {
    docs: {
      description: {
        component: '加载数据时显示动效。',
      },
    },
  },
  args: {},
  argTypes: {
    loading: {
      control: { type: 'select' },
      options: ['true', 'false'],
      description: '是否加载中',
      table: { defaultValue: { summary: 'false' } },
    },
    type: {
      control: { type: 'select' },
      options: ['fullscreen', 'part'],
      description: '加载范围',
    },
  },
};

const Template = ({ loading, type }) =>
  html`<cms-loading type=${type} loading=${loading}
    ><span slot="footer">loading....</span></cms-loading
  >`;

export const Fullscreen = Template.bind({});
Fullscreen.args = {
  type: 'fullscreen',
  loading: 'true',
};

export const Part = Template.bind({});
Part.args = {
  type: 'part',
  loading: 'true',
};
Part.decorators = [
  story =>
    html`<div style="position: relative; width: 200px; height: 300px; border: 1px solid red">
      ${story()}
    </div>`,
];
