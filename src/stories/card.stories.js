import { html } from 'lit-html';
import '../component/card/index';

export default {
  title: 'Web-Component/Card',
  parameters: {
    docs: {
      description: {
        component:
          '将信息聚合在卡片容器中展示。卡片包含标题，内容以及操作区域。Card 组件由`header`和`body`组成。`header`是可选的，其内容取决于一个具名的 slot。',
      },
    },
  },
  argTypes: {
    shadow: {
      control: { type: 'select' },
      options: ['always', 'hover', 'never'],
      description: '设置阴影显示时机',
      table: { defaultValue: { summary: 'always' } },
    },
  },
};

const Template = ({ shadow }) => html`<cms-card shadow=${shadow}>card组件展示</cms-card>`;

export const Default = Template.bind({});
Default.storyName = 'Default';
Default.args = {
  shadow: 'always',
};

export const ShadowAlways = Template.bind({});
ShadowAlways.storyName = 'Shadow: always';
ShadowAlways.args = {
  shadow: 'always',
};

export const ShadowHover = Template.bind({});
ShadowHover.storyName = 'Shadow: hover';
ShadowHover.args = {
  shadow: 'hover',
};

export const ShadowNever = Template.bind({});
ShadowNever.storyName = 'Shadow: never';
ShadowNever.args = {
  shadow: 'never',
};
