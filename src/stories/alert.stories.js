import { html } from 'lit-html';
import '../component/alert/index';
// alert组件属性：type,closeable
export default {
  title: 'Web-Component/Alert',
  parameters: {
    docs: {
      description: {
        component: '用于页面中展示重要的提示信息。Alert 组件不属于浮层元素，不会自动消失或关闭。',
      },
    },
  },
  argTypes: {
    type: {
      control: { type: 'radio' },
      options: ['default', 'info', 'success', 'warning', 'error'],
      description: 'Alert 类型',
      table: { defaultValue: { summary: 'default' } },
    },
    closable: {
      description: '是否可以关闭',
      table: { defaultValue: { summary: 'false' } },
    },
    slot: {
      description: '组件显示的文字内容',
    },
  },
};

const Template = ({ type, closable, slot }) =>
  html`<cms-alert type=${type} closable=${closable}>${slot}</cms-alert>`;

export const Default = Template.bind({});
Default.storyName = 'Default';
Default.args = {
  type: 'default',
  closable: false,
  slot: 'alert组件效果展示',
};

export const TypeSuccess = Template.bind({});
TypeSuccess.storyName = 'Type: success';
TypeSuccess.args = {
  type: 'success',
  closable: false,
  slot: 'alert组件效果展示',
};

export const TypeInfo = Template.bind({});
TypeInfo.storyName = 'Type: info';
TypeInfo.args = {
  type: 'info',
  closable: false,
  slot: 'alert组件效果展示',
};

export const TypeWarning = Template.bind({});
TypeWarning.storyName = 'Type: warning';
TypeWarning.args = {
  type: 'warning',
  closable: false,
  slot: 'alert组件效果展示',
};

export const TypeError = Template.bind({});
TypeError.storyName = 'Type: error';
TypeError.args = {
  type: 'error',
  closable: false,
  slot: 'alert组件效果展示',
};

export const Closable = Template.bind({});
Closable.storyName = 'Closable';
Closable.args = {
  type: 'default',
  closable: true,
  slot: 'alert组件效果展示',
};

export const ClosableSuccess = Template.bind({});
ClosableSuccess.storyName = 'Closable: success';
ClosableSuccess.args = {
  type: 'success',
  closable: true,
  slot: 'alert组件效果展示',
};

export const ClosableInfo = Template.bind({});
ClosableInfo.storyName = 'Closable: info';
ClosableInfo.args = {
  type: 'info',
  closable: true,
  slot: 'alert组件效果展示',
};

export const ClosableWarning = Template.bind({});
ClosableWarning.storyName = 'Closable: warning';
ClosableWarning.args = {
  type: 'warning',
  closable: true,
  slot: 'alert组件效果展示',
};

export const ClosableError = Template.bind({});
ClosableError.storyName = 'Closable: error';
ClosableError.args = {
  type: 'error',
  closable: true,
  slot: 'alert组件效果展示',
};
