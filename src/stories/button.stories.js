import { html } from 'lit-html';
import '../component/button/index';

export default {
  title: 'Web-Component/Button',
  parameters: {
    docs: {
      description: {
        component:
          '常用的操作按钮。可以使用`type`，`plain`，`round`来定义按钮样式，使用`disabled`属性来定义按钮是否被禁用。',
      },
    },
  },
  argTypes: {
    type: {
      control: { type: 'radio' },
      options: ['default', 'info', 'success', 'warning', 'danger'],
      description: '类型',
      table: { defaultValue: { summary: 'default' } },
    },
    round: {
      description: '是否为圆角按钮',
      table: { defaultValue: { summary: 'false' } },
    },
    plain: {
      description: '是否为朴素按钮',
      table: { defaultValue: { summary: 'false' } },
    },
    disabled: {
      description: '按钮是否为禁用状态',
      table: { defaultValue: { summary: 'false' } },
    },
  },
};

const Template = ({ type, round, plain, disabled }) =>
  html`<cms-button type=${type} round=${round} plain=${plain} disabled=${disabled}
    >CMS-BUTTON</cms-button
  >`;

export const Default = Template.bind({});
Default.args = {
  type: 'default',
  round: false,
  disabled: false,
  plain: false,
};

export const TypeSuccess = Template.bind({});
TypeSuccess.storyName = 'Type: success';
TypeSuccess.args = {
  type: 'success',
  round: false,
  disabled: false,
  plain: false,
};

export const TypeInfo = Template.bind({});
TypeInfo.storyName = 'Type: info';
TypeInfo.args = {
  type: 'info',
  round: false,
  disabled: false,
  plain: false,
};

export const TypeWarning = Template.bind({});
TypeWarning.storyName = 'Type: warning';
TypeWarning.args = {
  type: 'warning',
  round: false,
  disabled: false,
  plain: false,
};

export const TypeDanger = Template.bind({});
TypeDanger.storyName = 'Type: danger';
TypeDanger.args = {
  type: 'danger',
  round: false,
  disabled: false,
  plain: false,
};

export const Plain = Template.bind({});
Plain.storyName = 'Plain';
Plain.args = {
  type: 'default',
  round: false,
  disabled: false,
  plain: true,
};

export const PlainSuccess = Template.bind({});
PlainSuccess.storyName = 'Plain: success';
PlainSuccess.args = {
  type: 'success',
  round: false,
  disabled: false,
  plain: true,
};

export const PlainInfo = Template.bind({});
PlainInfo.storyName = 'Plain: info';
PlainInfo.args = {
  type: 'info',
  round: false,
  disabled: false,
  plain: true,
};

export const PlainWarning = Template.bind({});
PlainWarning.storyName = 'Plain: warning';
PlainWarning.args = {
  type: 'warning',
  round: false,
  disabled: false,
  plain: true,
};

export const PlainDanger = Template.bind({});
PlainDanger.storyName = 'Plain: danger';
PlainDanger.args = {
  type: 'danger',
  round: false,
  disabled: false,
  plain: true,
};

export const Round = Template.bind({});
Round.storyName = 'Round';
Round.args = {
  type: 'default',
  round: true,
  disabled: false,
  plain: false,
};

export const RoundSuccess = Template.bind({});
RoundSuccess.storyName = 'Round: success';
RoundSuccess.args = {
  type: 'success',
  round: true,
  disabled: false,
  plain: false,
};

export const RoundInfo = Template.bind({});
RoundInfo.storyName = 'Round: info';
RoundInfo.args = {
  type: 'info',
  round: true,
  disabled: false,
  plain: false,
};

export const RoundWarning = Template.bind({});
RoundWarning.storyName = 'Round: warning';
RoundWarning.args = {
  type: 'warning',
  round: true,
  disabled: false,
  plain: false,
};

export const RoundDanger = Template.bind({});
RoundDanger.storyName = 'Round: danger';
RoundDanger.args = {
  type: 'danger',
  round: true,
  disabled: false,
  plain: false,
};

export const Disabled = Template.bind({});
Disabled.storyName = 'Disabled';
Disabled.args = {
  type: 'default',
  round: false,
  disabled: true,
  plain: false,
};

export const DisabledSuccess = Template.bind({});
DisabledSuccess.storyName = 'Disabled: success';
DisabledSuccess.args = {
  type: 'success',
  round: false,
  disabled: true,
  plain: false,
};

export const DisabledInfo = Template.bind({});
DisabledInfo.storyName = 'Disabled: info';
DisabledInfo.args = {
  type: 'info',
  round: false,
  disabled: true,
  plain: false,
};

export const DisabledWarning = Template.bind({});
DisabledWarning.storyName = 'Disabled: warning';
DisabledWarning.args = {
  type: 'warning',
  round: false,
  disabled: true,
  plain: false,
};

export const DisabledDanger = Template.bind({});
DisabledDanger.storyName = 'Disabled: danger';
DisabledDanger.args = {
  type: 'danger',
  round: false,
  disabled: true,
  plain: false,
};

export const DisabledPlain = Template.bind({});
DisabledPlain.storyName = 'Disabled: plain';
DisabledPlain.args = {
  type: 'default',
  round: false,
  disabled: true,
  plain: true,
};

export const DisabledRound = Template.bind({});
DisabledRound.storyName = 'Disabled: round';
DisabledRound.args = {
  type: 'default',
  round: true,
  disabled: true,
  plain: false,
};
