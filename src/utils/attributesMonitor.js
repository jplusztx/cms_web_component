/**
 *
 * @param {object<Array>} typeArr={key:value}
 * key:为此组件内的自定义属性
 * value:为对应key可取的值集合
 * 示例：
 *      typeArr = {type:['default', 'part', 'fullscreen'],
          loading:['false','true']
        };
 */

// 通过bind绑定,传过来的
function attributeMonitor(name, last, cur, typeArr) {
  //找到(name==key)的属性，判断key对应的数组里面存不存在'false'||'true'；如果存在就调用下面的代码
  if (Object.hasOwn(typeArr, name) && typeArr.name.findIndex(cur => cur == 'false' || 'true')) {
    if (cur === null || cur === 'false' || (typeof cur === 'boolean' && !cur)) {
      this.props.loading = false;
    } else {
      this.props.loading = true;
    }
  }
  //如果不存在，就调用下面的代码
  else if (name === 'type') {
    this.props.type = typeArr.type.includes(cur) ? cur : 'default';
  }

  this.renderStyle();
}

export default attributeMonitor;
