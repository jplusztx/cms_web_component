/**
 *
 * @param {HTMLElement} dom
 * @return 获取当前元素上下左右四条边到body上侧和左侧的距离
 */
export function getOffset(dom) {
  const width = dom.offsetWidth;
  const height = dom.offsetHeight;
  let left = 0;
  let top = 0;
  while (dom !== document.body) {
    left += dom.offsetLeft;
    top += dom.offsetTop;
    dom = dom.offsetParent;
  }
  return { left, top, right: left + width, bottom: top + height };
}

/**
 *
 * @param {string} id
 * @param {object} styleOptions
 * @returns
 */
export function createModal(id, styleOptions) {
  let modal = null;
  const show = () => {
    modal = document.querySelector(`#${id}`);
    if (!modal) {
      modal = document.createElement('div');
      modal.id = id;
      Object.assign(modal.style, styleOptions);
      document.body.appendChild(modal);
    }
    return modal;
  };
  const remove = () => {
    if (modal && modal.childNodes.length === 0) {
      modal.remove();
    }
  };
  return { show, remove };
}

/**
 * 计算某个DOM元素，在屏幕中展示的位置到body左上角的距离
 * @param {HTMLElement} reference 被计算的元素
 * @param {function} fnc 回调函数
 * @returns {function} 用于：移除所有的监听事件
 */
export function getDOM2Body(reference, fnc) {
  const referenceOffset = getOffset(reference);
  const eventElArr = [];
  let p = reference.parentElement;
  function setView(e) {
    fnc({
      left: referenceOffset.left - e.target.scrollLeft,
      top: referenceOffset.top - e.target.scrollTop,
      right: referenceOffset.right - e.target.scrollLeft,
      bottom: referenceOffset.bottom - e.target.scrollTop,
    });
  }
  while (p) {
    if (p === document.body) {
      break;
    }
    if (p.scrollHeight > p.clientHeight) {
      p.addEventListener('scroll', setView);
      eventElArr.push(p);
    }
    p = p.parentElement;
  }
  return () => {
    eventElArr.forEach(i => i.removeEventListener('scroll', setView));
  };
}
