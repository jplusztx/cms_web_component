import colorTransform from './colorTransform';

/**
 *
 * @param {'default' | 'success' | 'info' | 'warning' | 'danger' | 'error'} type
 * @param {'deep' | 'light' | 'default'} style
 * @param {number} alpha
 * @returns
 */
function getColor(type, style = 'default', alpha = 1) {
  const globalColor = {
    primary: 'rgba(201,0,13,1)',
    gray: 'rgba(192,196,204,1)',
    default: 'rgba(64,158,255,1)',
    success: 'rgba(103,194,58,1)',
    info: 'rgba(144,147,153,1)',
    warning: 'rgba(230,162,60,1)',
    danger: 'rgba(245,108,108,1)',
    error: 'rgba(255,0,0,1)',
  };
  const color = globalColor[type] ?? globalColor.default;
  return colorTransform(color, style, alpha);
}

export default getColor;
