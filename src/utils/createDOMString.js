/**
 *
 * @param {object ｜ undefined} props
 * @returns
 */
function createDOMString(props = {}) {
  if (typeof props === 'object' && !Array.isArray(props)) {
    return function (args, ...func) {
      return args.reduce((res, cur, i) => {
        let insert = func[i - 1] ? (typeof func[i - 1] === 'function' ? func[i - 1](props) : func[i - 1])  : '';
        return res + insert + cur;
      });
    };
  } else {
    return props.reduce((res, cur) => res + cur);
  }
}
/**
 * 如果需要插入函数进行使用：
 * 使用方式必须是：createDOMString(参数可不传或者为对象)``
 *
 * 不需要使用参数时，可以直接使用：createDOMString``
 *
 */

/**
 * 这部分代码不需要懂，可以查阅：ES6的标签函数语法
 * https://zhuanlan.zhihu.com/p/31687266
 *
 * 此处就是对标签函数的使用进行一次简单的扩展：将每个插值处改为传入函数使用。
 * 可以查看npm包：styled-component 的用法，实现了它的部分简易功能
 *
 */

export default createDOMString;
