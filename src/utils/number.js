export function toNumOrDefault(num, defaultVAlue) {
  const res = +num;
  return res === res ? res : defaultVAlue;
}

export function inRange(num, min = -Infinity, max = +Infinity) {
  num = toNumOrDefault(num, 0);
  min = toNumOrDefault(min, -Infinity);
  max = toNumOrDefault(max, +Infinity);
  if (num < min) return min;
  if (num > max) return max;
  return num;
}
