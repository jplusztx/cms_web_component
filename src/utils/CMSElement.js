/**
 * 扩展HTMLElement的属性：
 */

class CMSElement extends HTMLElement {
  // 保存shadow dom
  _sd;
  /**
   * 定义组件库中公共使用的变量：
   *  1. 颜色值
   */
  global = {
    color: {
      primary: '#c9000d',
      gray: '#c0c4cc',
      default: 'rgba(64,158,255,1)', //getColor('rgb(64,158, 255)'),
      success: 'rgba(103,194,58,1)',
      info: 'rgba(144,147,153,1)',
      warning: 'rgba(230,162,60,1)',
      danger: 'rgba(245,108,108,1)',
    },
  };
}

export default CMSElement;
