import { createModal, getDOM2Body } from './dom';

const TOOLTOPS_MODAL_ID = 'cms-tooltips-modal';
/**
 *
 * @param {HTMLElement} reference 被跟踪的元素
 * @param {HTMLElement} tooltips 需要弹出的元素
 * @return {function}
 */
function createTooltips(reference, tooltips) {
  const modalStyle = { position: 'absolute', left: '0px', top: '0px' };

  const modalObj = createModal(TOOLTOPS_MODAL_ID, modalStyle);
  tooltips.remove();

  function setPos({ x, y }) {
    if (!x || !y) return;
    Object.assign(tooltips.style, {
      transform: `translate(${x}px, ${y}px)`,
    });
  }
  function show() {
    modalObj.show().appendChild(tooltips);
  }
  function hide() {
    tooltips.remove();
    modalObj.remove();
  }

  return { show, hide, setPos };
}

export default createTooltips;
