import { toNumOrDefault } from '../utils/number';

test('test toNumOrDefault', () => {
  expect(toNumOrDefault(2)).toBe(2);
  expect(toNumOrDefault('12334')).toBe(12334);
  expect(toNumOrDefault('-1')).toBe(-1);
  expect(toNumOrDefault('+123')).toBe(123);
  expect(toNumOrDefault('asd', 100)).toBe(100);
});
