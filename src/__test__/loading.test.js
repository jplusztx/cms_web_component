beforeEach(async () => {
  await import('../component/loading');
});

describe('测试loading组件', () => {
  document.body.innerHTML = '<cms-loading type="part"></cms-loading><button>toggle</button>';
  const btn = document.querySelector('button');
  const load = document.querySelector('cms-loading');
  btn.addEventListener('click', () => {
    load.toggleAttribute('loading');
  });
  test('load ~', () => {
    btn.click();
    expect(load.hasAttribute('loading')).toBe(true);
  });
  test('no load ~', () => {
    btn.click();
    expect(load.hasAttribute('loading')).toBe(false);
  });
});
