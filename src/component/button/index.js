import createDOMString from '../../utils/createDOMString';
import createElement from '../../utils/createElement';
import getColor from '../../utils/getColor';

function getClassList({ plain, disabled }) {
  const classList = ['button'];
  plain && classList.push('plain');
  disabled && classList.push('is-disabled');
  return classList.join(' ');
}

createElement({
  name: 'cms-button',
  props: {
    type: cur => {
      const typeArr = ['success', 'info', 'warning', 'danger', 'default'];
      return typeArr.includes(cur) ? cur : 'default';
    },
    round: cur => !(cur === null || cur === 'false'),
    plain: cur => !(cur === null || cur === 'false'),
    disabled: cur => !(cur === null || cur === 'false'),
  },
  defaultProps: {
    type: 'default',
    round: false,
    plain: false,
    disabled: false,
  },
  render(props) {
    return createDOMString(props)`
      <style>
        :host {
          --scale: 0.85;
          --edge: 50px;
          --text-color: #fff;
          --bg-color:  ${getColor(props.type)};
          --border-color: ${getColor(props.type)};
          display: inline-block;
        }
        :host > .button {
          position: relative;
          z-index: 0;
          overflow: hidden;
          line-height: 1;
          height: 32px;
          white-space: nowrap;
          cursor: pointer;
          text-align: center;
          color: var(--text-color);
          font-weight: 500;
          font-size: 14px;
          padding: 8px 15px;
          background-color: var(--bg-color);
          border: 1px solid #dcdfe6;
          border-radius: ${props => (props.round ? '999px' : '4px')};
          border-color:  var(--border-color);
        }

        :host >.button:hover {
          --bg-color:   ${getColor(props.type, 'light')};
          --border-color:  ${getColor(props.type, 'light')};
        }
        :host >.button:active {
          --bg-color:   ${getColor(props.type, 'deep')};
          --border-color:  ${getColor(props.type, 'deep')};
        }

        :host > .button.plain {
          --bg-color:   ${getColor(props.type, 'default', 0.1)};
          --border-color: ${getColor(props.type)};
          --text-color: ${getColor(props.type)};
        }
        :host > .button.plain:hover {
          --bg-color:  ${getColor(props.type)};
          --text-color: #fff;
        }
        :host > .button.plain:active {
          --bg-color:   ${getColor(props.type, 'deep')};
          --border-color:  ${getColor(props.type, 'deep')};
          --text-color: #fff;
        }
        :host > .button.is-disabled {
          cursor: not-allowed;
          --bg-color:   ${getColor(props.type, 'light')};
          --border-color:  ${getColor(props.type, 'light')};
        }
        :host > .button.is-disabled:hover {
          cursor: not-allowed;
          --text-color: #fff;
          --bg-color:   ${getColor(props.type, 'light')};
          --border-color:  ${getColor(props.type, 'light')};
        }
        :host > .button.plain.is-disabled {
          cursor: not-allowed;
          --text-color: ${getColor(props.type, 'light')};
          --bg-color:   ${getColor(props.type, 'light', 0.1)};
          --border-color:  ${getColor(props.type, 'light')};
        }
        :host > .button.is-disabled::after {
          content: "";
          display: block;
          position: absolute;
          width: 100%;
          height: 100%;
          left: 0;
          top: 0;
          z-index: 1000;
          cursor: not-allowed;
        }
      </style>
      <button class="${getClassList}">
          <slot></slot>
      </button>
    `;
  },
});
