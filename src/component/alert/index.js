import createDOMString from '../../utils/createDOMString';
import createElement from '../../utils/createElement';
import getColor from '../../utils/getColor';
import * as alertIcons from './icon';

const getClosable = props => {
  return props.closable ? '' : 'none';
};

createElement({
  name: 'cms-alert',
  props: {
    type: cur => {
      const typeArr = ['default', 'success', 'info', 'warning', 'error'];
      return typeArr.includes(cur) ? cur : 'default';
    },
    closable: cur => {
      return typeof cur === 'string' && cur !== 'false';
    },
  },
  defaultProps: {
    type: 'default',
    closable: false,
  },
  events(target, addEvent) {
    addEvent('.alert-close-icon', 'click', () => {
      target.remove();
    });
  },
  render(props) {
    return createDOMString(props)`
      <style>
        :host {
          --scale: 0.85;
          --edge: 50px;
          display: block;
        }
        :host > .alert{
          color: ${getColor(props.type, 'light', 1)};
          border:1px solid ${getColor(props.type, 'light', 1)};
          background-color: ${getColor(props.type, 'light', 0.1)};
          display: flex;
          justify-content:space-between;
          align-items: center;
          box-sizing: border-box;
          white-space: nowrap;
          font-size: 14px;
          padding: 8px;
          margin:10px 0;
          border-radius: 5px;
          width: fit-content;
        }
        :host .alert-content {
          display: flex;
          align-items: center;
        }
        :host .alert-content svg {
          align-self: flex-start;
          margin-right: 5px;
        }
        :host .alert-close-icon{
          display:${getClosable};
          margin-left: 12px;
          margin-top: 2px;
          padding: 0;
          overflow: hidden;
          font-size: 14px;
          line-height: 12px;
          align-self: flex-start;
          background-color: transparent;
          border: none;
          outline: none;
          cursor: pointer;
        }
        :host .alert-close-icon svg{
          height: 1em;
          width: 1em;
          color: #a8abb2;
        }
        :host .alert-message{
          margin-left: 5px;
        }
      </style>
      <div class='alert'>
        <div class="alert-content">
          ${alertIcons[props.type + 'Icon']}
        <span class="alert-message">
          <slot></slot>
        </span>
        </div>
        <button class="alert-close-icon">
          ${alertIcons['closeIcon']}
        </button>
      </div>
    `;
  },
});
