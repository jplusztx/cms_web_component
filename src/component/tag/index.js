import createDOMString from '../../utils/createDOMString';
import getColor from '../../utils/getColor';
import createElement from '../../utils/createElement';

const getHeight = props => {
  switch (props.size) {
    case 'default':
      return '24px';
    case 'large':
      return '32px';
    case 'small':
      return '20px';
  }
};

const getPadding = props => {
  switch (props.size) {
    case 'default':
      return '9px';
    case 'large':
      return '11px';
    case 'small':
      return '7px';
  }
};

const getClosable = props => {
  return props.closable === 'true' || props.closable ? 'inline-flex' : 'none';
};

createElement({
  name: 'cms-tag',
  props: {
    type: cur => {
      const typeArr = ['success', 'info', 'warning', 'danger', 'default'];
      return typeArr.includes(cur) ? cur : 'default';
    },
    size: cur => {
      const typeArr = ['large', 'small', 'default'];
      return typeArr.includes(cur) ? cur : 'default';
    },
    round: cur => {
      return !(cur === null || cur === 'false' || (typeof cur === 'boolean' && !cur));
    },
    closable: cur => {
      return !(cur === null || cur === 'false' || (typeof cur === 'boolean' && !cur));
    },
  },
  defaultProps: {
    type: 'default',
    size: 'default',
    round: false,
    closable: false,
  },
  events: (target, addEvent, dispatch) => {
    addEvent('.tag-close-icon', 'click', e => {
      target.remove();
    });
  },
  render(props) {
    return createDOMString(props)`
      <style>
        :host {
          --scale: 0.85;
          --edge: 50px;
          display: inline-block;
          font-weight: 400;
        }
        :host > .tag {
          color: ${getColor(props.type)};
          background-color: ${getColor(props.type, 'default', 0.1)};
          display: inline-flex;
          justify-content: center;
          align-items: center;
          box-sizing: border-box;
          white-space: nowrap;
          line-height: 1;
          border: solid 1px ${getColor(props.type, 'default', 0.2)};
          height: ${getHeight};
          padding: 0 ${getPadding};
          border-radius: ${props => (props.round ? '9999px' : '4px')};
          font-size: 12px;
        }
        :host > .tag >.tag-close-icon{
          display:${getClosable};
          margin-left: 6px;
          padding: 0;
          overflow: hidden;
          justify-content: center;
          align-items: center;
          font-size: 14px;
          cursor: pointer;
        }
        :host .tag-close-icon svg{
          height: 1em;
          width: 1em;
          color: ${getColor(props.type)};
        }
      </style>    
      <span class='tag'>
        <span class='tag-content'>
          <slot></slot>
        </span>
        <div class="tag-close-icon">
          <svg viewBox="0 0 1024 1024" xmlns="http://www.w3.org/2000/svg"><path fill="currentColor" d="M764.288 214.592 512 466.88 259.712 214.592a31.936 31.936 0 0 0-45.12 45.12L466.752 512 214.528 764.224a31.936 31.936 0 1 0 45.12 45.184L512 557.184l252.288 252.288a31.936 31.936 0 0 0 45.12-45.12L557.12 512.064l252.288-252.352a31.936 31.936 0 1 0-45.12-45.184z"></path></svg>
        </div>
      </span>
    `;
  },
});
