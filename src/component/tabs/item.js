import createDOMString from '../../utils/createDOMString';
import createElement from '../../utils/createElement';

createElement({
  name: 'cms-tabs-item',
  props: {
    label: cur => {
      if (cur === null || cur.trim() === '') {
        return 'Default Label';
      } else {
        return cur.trim();
      }
    },
  },
  defaultProps: {},
  render(props) {
    return createDOMString(props)`
      <style>
      :host {
        box-sizing: border-box;
        padding: 10px;
      };
      </style>
      <div class="cms-tabs-item">
        <slot></slot>
      </div>
    `;
  },
});
