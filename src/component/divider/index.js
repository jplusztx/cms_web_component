import createElement from '../../utils/createElement';
import createDOMString from '../../utils/createDOMString';

const getTransform = props => {
  switch (props['content-position']) {
    case 'left':
      return 'translateY(-50%)';
    case 'center':
      return 'translate(-50%) translateY(-50%)';
    case 'right':
      return 'translateY(-50%)';
  }
};

const getPosition = props => {
  switch (props['content-position']) {
    case 'left':
      return 'left: 20px';
    case 'center':
      return 'left: 50%';
    case 'right':
      return 'right: 20px';
  }
};

createElement({
  name: 'cms-divider',
  props: {
    'content-position': cur => {
      const cpArr = ['left', 'center', 'right'];
      return cpArr.includes(cur) ? cur : 'left';
    },
    'border-style': cur => {
      const bsArr = ['solid', 'dashed'];
      return bsArr.includes(cur) ? cur : 'solid';
    },
    direction: cur => {
      const dirArr = ['vertical', 'horizontal'];
      return dirArr.includes(cur) ? cur : 'horizontal';
    },
  },
  defaultProps: {
    'content-position': 'center',
    'border-style': 'solid',
    direction: 'horizontal',
  },
  render(props) {
    return createDOMString(props)`
      <style>
      :host {
        --scale: 0.85;
        --edge: 50px;
      }
      :host >.horizontal {
        display: block;
        position: relative;
        height: 1px;
        width: 100%;
        margin: 24px 0;
        border-top: 1px #dcdfe6 ${props['border-style']};
      }
      :host >.vertical{
        display: inline-block;
        width: 1px;
        height: 1em;
        margin: 0 8px;
        vertical-align: middle;
        position: relative;
        border-left: 1px #dcdfe6 ${props['border-style']};
      }
      :host >.horizontal >.divider_text {
        position: absolute;
        background-color: #ffffff;
        padding: 0 20px;
        font-weight: 500;
        color: #303133;
        font-size: 14px;
      }
      :host >.horizontal >.position {
        transform: ${getTransform};
        ${getPosition};
      }
      </style>
      <div class="${props => props.direction}">
        <div class="divider_text position">
            ${props => (props.direction === 'horizontal' ? '<slot></slot>' : '')}
        </div>
      </div>
    `;
  },
});
