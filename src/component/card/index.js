import createDOMString from '../../utils/createDOMString';
import createElement from '../../utils/createElement';

function getClassList({ shadow }) {
  const classList = ['card'];
  shadow === 'always' && classList.push('is-always-shadow');
  shadow === 'hover' && classList.push('is-hover-shadow');
  return classList.join(' ');
}

createElement({
  name: 'cms-card',
  props: {
    shadow: cur => (['always', 'hover', 'never'].includes(cur) ? cur : 'always'),
  },
  defaultProps: {
    shadow: '',
  },
  mounted(target) {
    const slotHead = target.querySelectorAll(':scope > [slot=head]');
    if (slotHead.length === 0) {
      target.shadowRoot.querySelector('#head').classList.remove('card-head');
    }
  },
  render(props) {
    return createDOMString(props)`
    <style>
      :host > .card{
        border-radius: 4px;
        border: 1px solid #e4e7ed;
        background-color: #fff;
        overflow: hidden;
        color: #303133;
        transition: .3s;
      }
      :host .card-head{
        padding: 18px 20px;
        border-bottom: 1px solid #e4e7ed;
        box-sizing: border-box;
      }
      :host .card-body{
        padding: 20px;
      }
      :host .card.is-always-shadow{
        box-shadow: 0px 0px 12px rgba(0, 0, 0, .12);
      }
      :host .card.is-hover-shadow:hover{
        box-shadow: 0px 0px 12px rgba(0, 0, 0, .12);
      }
    </style>
    <div class="${getClassList}">
      <div class="card-head" id="head">
        <slot name="head"></slot>
      </div>
      <div class="card-body">
        <slot></slot>
      </div>
    </div>
    `;
  },
});
