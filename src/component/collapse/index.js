import createDOMString from '../../utils/createDOMString';
import createElement from '../../utils/createElement';
import { arrowDown } from './icon';

createElement({
  name: 'cms-collapse',
  props: {
    disabled: (cur, _) => !(cur === null || cur === 'false'),
    display: (cur, _, target) => {
      const res = !(cur === null || cur === 'false');
      setTimeout(() => {
        target.style.setProperty('--r', res ? '0deg' : '-90deg');
        target.style.setProperty('--h', res ? target._rawHeight : '0px');
      }, 16.666666);
      return res;
    },
  },
  defaultProps: {
    disabled: false,
    display: true,
  },
  events(target, addEvent) {
    addEvent('.cms-collapse-arrow', 'click', () => {
      target.display = !target.display;
      // let dom =  document.createElement('div');
      // dom.innerHTML =
        // '测试测测试测hii殴打hi的话覅上的覅测试测测试测hii殴打hi的话覅上的覅测试测测试测hii殴打hi的话覅上的覅测试测测试测hii殴打hi的话覅上的覅测试测测试测hii殴打hi的话覅上的覅测试测测试测hii殴打hi的话覅上的覅测试测测试测hii殴打hi的话覅上的覅测试测测试测hii殴打hi的话覅上的覅测试测测试测hii殴打hi的话覅上的覅测试测测试测hii殴打hi的话覅上的覅测试测测试测hii殴打hi的话覅上的覅';
      // target.appendChild(dom);
    });
    addEvent('slot', 'slotchange',() => {
      const inner = target.shadowRoot.querySelector('.cms-collapse-content_inner');
      let height = `${inner.scrollHeight}px`;
      if (height === target._rawHeight || target.props.display === false) return;
      target._rawHeight = height;
      target.style.setProperty('--h', target.props.display ? target._rawHeight : '0px');
    })
  },
  mounted(target) {
    setTimeout(() => {
      const inner = target.shadowRoot.querySelector('.cms-collapse-content_inner');
      target._rawHeight = inner.scrollHeight + 'px';
      const callback = function() {
        const inner = target.shadowRoot.querySelector('.cms-collapse-content_inner');
        let height = `${inner.scrollHeight}px`;
        if (height === target._rawHeight || target.props.display === false ) return
        target._rawHeight = height;
        target.style.setProperty('--h', target.props.display ? target._rawHeight : '0px');
      }
      window.addEventListener('resize', callback)
    });
  },
  render(props) {
    return createDOMString(props)`
      <style>
        :host {
          width:100%;
          height:100%;
        }
        :host .cms-collapse {
          position:relative;
          box-sizing:border-box;
          cursor:pointer;
          ${props.disabled ? 'filter: contrast(0.5);' : ''}
          overflow:hidden;
        }
        :host .cms-collapse-mask {
          position:absolute;
          ${props.disabled ? '' : 'display:none;'}
          width: 100%;
          height: 100%;
          cursor: not-allowed;
          z-index:2;
        }
        :host .cms-collapse-head {
          display:flex;
          justify-content: space-between;
          min-height:30px;
        }
        :host .cms-collapse-head .cms-collapse-arrow {
          height:25px;
          transform: rotate(var(--r));
          transition: transform .3s linear;
          cursor: pointer;
        }
        :host .cms-collapse-content {
          overflow: hidden;
          height: var(--h);
          transition: height .2s linear;
        }
      </style>
      <div class='cms-collapse'>
          <div class='cms-collapse-mask'></div>
          <div class='cms-collapse-head'>
              <slot name='head'>Default Title</slot>
              <i class='cms-collapse-arrow'>
                  ${arrowDown}
              </i>
          </div>
          <div class='cms-collapse-content'>
            <div class='cms-collapse-content_inner'>
              <slot></slot>
            </div>
          </div>
      </div>
    `;
  },
});
