import CMSElement from '../../utils/CMSElement';
import createDOMString from '../../utils/createDOMString';
import getColor from '../../utils/getColor';

/**
 * @param {string} type 类型['success', 'info', 'warning', 'danger', 'default']
 * @param {boolean} underline 下划线
 * @param {boolean} disabled 禁用
 */
class CmsLink extends CMSElement {
  props = {
    type: 'default',
    underline: true,
    disabled: false,
  };

  constructor() {
    super();
    this._sd = this.attachShadow({ mode: 'closed' });
    this.renderStyle();
    this.renderDOM();
  }

  renderDOM() {
    const template = document.createElement('template');
    template.innerHTML = createDOMString()`
    <a class="link">
        <span class="link-inner">
            <slot></slot>
        </span>
    </a>
    `;
    this._sd.appendChild(template.content.cloneNode(true));
  }

  renderStyle() {
    let style = this._sd.querySelector('style');
    if (!style) {
      style = document.createElement('style');
      this._sd.appendChild(style);
    }
    const styleProps = { ...this.props, ...this.global };

    style.innerHTML = createDOMString(styleProps)`
    a {
        text-decoration: none;
        background-color: transparent;
    }
    :host > .link {
        margin-right: 8px;
        justify-content: center;
        vertical-align: middle;
        position: relative;
        text-decoration: none;
        outline: none;
        cursor: pointer;
        padding: 0;
        font-size: 14px;
        font-weight: 500;
        color: ${getColor(this.props.type)};
    }
    :host> .link:hover{
        color: ${getColor(this.props.type, 'light')};
    }
    :host> .link:active{
        color: ${getColor(this.props.type, 'light')};
    }
     :host .link-inner {
        display: inline-flex;
        justify-content: center;
        border-bottom: 1px solid transparent;
     }
    :host .link-inner.is-underlined:hover{
        border-bottom: 1px solid ${getColor(this.props.type)};
    }
    :host>.link.is-disabled{
        color: ${getColor(this.props.type, 'light')};
        cursor: not-allowed;
    }
    `;
  }

  static get observedAttributes() {
    return ['type', 'underline', 'disabled', 'href'];
  }
  set disabled(val) {
    this.setAttribute('disabled', val);
  }
  attributeChangedCallback(name, last, cur) {
    if (name === 'type') {
      const typeArr = ['success', 'info', 'warning', 'danger', 'default'];
      this.props.type = typeArr.includes(cur) ? cur : 'default';
    } else if (name === 'underline') {
      if (this.hasAttribute('underline')) {
        this.props.underline = true;
        let element = this._sd.querySelector('.link-inner');
        element.classList.add('is-underlined');
      } else {
        this.props.underline = false;
        let element = this._sd.querySelector('.link-inner');
        element.classList.remove('is-underlined');
      }
    } else if (name === 'disabled') {
      if (cur === null || cur === 'false' || (typeof cur === 'boolean' && !cur)) {
        this.props.disabled = false;
        let element = this._sd.querySelector('.link');
        element.classList.remove('is-disabled');
      } else {
        this.props.disabled = true;
        let element = this._sd.querySelector('.link');
        element.classList.add('is-disabled');
      }
    } else if (name === 'href') {
      this._sd.querySelector('a').setAttribute('href', cur);
    }
    this.renderStyle();
  }
}

customElements.define('cms-link', CmsLink);
